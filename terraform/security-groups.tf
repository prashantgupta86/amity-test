
resource "aws_security_group" "test-worker_group_mgmt_one" {
  name_prefix = "test-worker_group_mgmt_one"
  vpc_id      = module.vpc.vpc_id

  ingress {
    from_port = 22
    to_port   = 22
    protocol  = "tcp"

    cidr_blocks = [
      "172.30.0.0/16",
    ]
  }
}
