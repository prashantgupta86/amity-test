variable "region" {
  default     = "ap-southeast-1"
  description = "AWS region"
}

provider "aws" {
  version = ">= 2.28.1"
  region  = "ap-southeast-1"
}

data "aws_availability_zones" "available" {}

locals {
  cluster_name = "test-nonprod-eks"
}


module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "2.6.0"

  name                 = "test-vpc-eks"
  cidr                 = "172.30.0.0/16"
  azs                  = data.aws_availability_zones.available.names
  private_subnets      = ["172.30.1.0/24", "172.30.2.0/24"]
  public_subnets       = ["172.30.101.0/24"]
  enable_nat_gateway   = true
  single_nat_gateway   = true
  enable_dns_hostnames = true

  tags = {
    "kubernetes.io/cluster/${local.cluster_name}" = "shared"
  }

  private_subnet_tags = {
    "kubernetes.io/cluster/${local.cluster_name}" = "shared"
    "kubernetes.io/role/internal-elb"             = "1"
  }

  public_subnet_tags = {
    "kubernetes.io/cluster/${local.cluster_name}" = "shared"
    "kubernetes.io/role/elb"                      = "1"
  }
}
