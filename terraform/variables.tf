variable "map_users" {
  description = "Additional IAM users to add to the aws-auth configmap."
  type = list(object({
    userarn  = string
    username = string
    groups   = list(string)
  }))

  default = [
    {
      userarn  = "arn:aws:iam::xxxxxxxx:user/test"
      username = "test"
      groups   = ["system:masters"]
    },
    ]
}
